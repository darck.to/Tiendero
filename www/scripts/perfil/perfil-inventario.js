$(function() {
  //CARGAMOS EL NUMERO DE CATEGORIAS DISPONIBLES Y EL LISTADO
  cargaInventario();

  function cargaInventario(e) {
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user")
    $.ajax({
      type: 'POST',
      url: 'http://localhost/tiendero/www/php/inventario/inventario-carga.php',
      data: {
        auth : tie_key,
        user : tie_user
      },
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false
    })
    .done(function(data) {
      var content = '';
      $.each(data, function (name, value) {
        content += '<tr>'
        if (value.num) {
          $('#invNumbers').html(value.num)
        } else if (value.success) {
          content += '<td class="has-text-centered"><input type="checkbox" name="pro-' + value.pro + '"></td>';
          content += '<td class="has-text-centered categoria-edita" val="' + value.pro + '" nom="' + value.nom + '" cat="' + value.cat + '" pre="' + value.pre + '" can="' + value.can + '" ord="' + value.ord + '" ped="' + value.ped + '"><i class="far fa-edit handed"></i></td>';
          content += '<td>' + value.nom + '</td>';
          content += '<td>' + value.cat + '</td>';
          content += '<td>' + value.can + '</td>';
          content += '<td>' + value.pre + '</td>'
          content += '<td class="has-text-centered categoria-borra" val="' + value.pro + '" "><i class="far fa-trash-alt handed"></i></td>'
        } else {
          content += '<td>No Dato</td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>'
        }
        content += '</tr>';
      })
      $('#tablaContenidoInventario').html(content);
      //EDITA CATEGORIA
      $('.categoria-edita').on('click', function(event) {
        ioCategorias(2,$(this).attr('val'),$(this).attr('nom'),$(this).attr('cat'),$(this).attr('pre'),$(this).attr('can'),$(this).attr('ord'),$(this).attr('ped'))
      });
      //BORRA CATEGORIA
      $('.categoria-borra').on('click', function(event) {
        ioCategorias(3,$(this).attr('val'))
      });
    })
    .fail(function() {
      console.log("error");
    })
  }

  //CATEGORíA NUEVA
  $('.categoria-nuevo').on('click', function(event) {
    event.preventDefault();
    ioCategorias(0)
  });

  //FUNCTION CREA/MODIFICA CATEGORIAS
  function ioCategorias(io, ide, nome, cate, pree, cane, orde, pede) {
    //RECIBIMOS LA FUENTE PARA 0 O 1
    var id = (io === 0) ? "" : ide;
    var nom = (io === 0) ? "" : nome;
    var cat = (io === 0) ? "" : cate;
    var pre = (io === 0) ? "" : pree;
    var can = (io === 0) ? "" : cane;
    var ord = (io === 0) ? "" : orde;
    var ped = (io === 0) ? "" : pede;
    var tagTitle = (io === 0) ? 'Nueva' : 'Edita';
    var tagSave = (io === 0) ? 'Guardar' : 'Edita';
    //INTERFAZ
    if (io === 3) {
      var box = '<h1>Borrar Producto</h1>';
      box += '<div class="field">';
        box += '<div class="control">';
        box += '<label class="label">Esta acción es irreversible</label>';
          box += '<input id="confirmButton" class="button is-danger" type="submit" value="Confirmar">';
        box += '</div>';
      box += '</div>';
    } else {
      var box = '<h1>' + tagTitle + ' Procucto</h1>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="nameProduct" class="input" type="text" placeholder="Nombre del Producto" value="' + nom + '">';
          box += '<input id="proId" type="hidden" value="' + id + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="cateProduct" class="input" type="text" placeholder="Categoria del Producto" value="' + cat + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="priceProduct" class="input" type="number" placeholder="Precio del Producto" value="' + pre + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="canProduct" class="input" type="text" placeholder="Cantidad en Inventario" value="' + can + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="ordProduct" class="input" type="text" placeholder="Cantidad en Ordenes de Salida" value="' + ord + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="pedProduct" class="input" type="text" placeholder="Cantidad en Pedidos de Entrada" value="' + ped + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="confirmButton" class="button is-danger" type="submit" value="' + tagSave + '">';
        box += '</div>';
      box += '</div>';
    }
    modal(box);

    //GUARDA CATEGORIA
    $('#confirmButton').on('click', function(event) {
      event.preventDefault();
      var nom = $('#nameProduct').val();
      var cat = $('#cateProduct').val();
      var pri = $('#priceProduct').val();
      var can = $('#canProduct').val();
      var ord = $('#ordProduct').val();
      var ped = $('#pedProduct').val();
      var formData = new FormData();
      formData.append('nom', nom);
      formData.append('cat', cat);
      formData.append('pre', pri);
      formData.append('can', can);
      formData.append('ord', ord);
      formData.append('ped', ped);
      formData.append('id', id);
      formData.append('io', io);
      formData.append('auth', localStorage.getItem("tie_key"));
      formData.append('user', localStorage.getItem("tie_user"))
      $.ajax({
        type: 'POST',
        url: 'http://localhost/tiendero/www/php/inventario/inventario-nueva.php',
        data: formData,
        processData: false,
        contentType: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false
      })
      .done(function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            xmodal();
            cargaInventario()
          } else {
            toast(value.message)
          }
        })
      })
      .fail(function() {
        console.log("error");
      })
    });
  }
});
