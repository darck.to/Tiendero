$(function() {
  //AUTOCOMPLETE OFF
  $('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //CARGA LA INFORMACION DEL PERFIL
  cargaPerfil()

  function cargaPerfil(e) {
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user")
    $.ajax({
      type: 'POST',
      url: 'http://localhost/tiendero/www/php/perfil/perfil-carga.php',
      data: {
        auth : tie_key,
        user : tie_user
      },
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false
    })
    .done(function(data) {
      $.each(data, function (name, value) {
        if (value.success) {
          $('#profileName').html(value.nom + ' ' + value.ape + ' ' + value.apm);
          $('#profileUser').html('@' + value.usr);
          $('#inputName').val(value.nom);
          $('#inputFirst').val(value.ape);
          $('#inputSecond').val(value.apm);
          $('#inputUser').val(value.usr);
          $('#inputEmail').val(value.mai)
          $('#inputTelephone').val(value.tel);
        } else {
          toast(value.message)
        }
      })
    })
    .fail(function() {
      console.log("error");
    })
  }

  //EDITAMOS EL USUARIO
  $('#formProfileEdit').submit(function(event) {
    event.preventDefault();
    var formData = new FormData(document.getElementById("formProfileEdit"));
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user")
    formData.append('auth',tie_key);
    formData.append('user', tie_user);
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');
    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    request.done(function(data) {
      $.each(data, function (name, value) {
        if (value.success) {
          toast(value.message)
        } else {
          toast(value.message)
        }
      })
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      //$('#formProfileEdit').trigger('reset');
    })
  })

  //EMAIL
  $('#inputEmail').on('keyup', function() {
    email = $(this).val();
    if (validateEmail(email)) {
      $('#saveButton').prop('disabled',false);
    } else {
      $('#saveButton').prop('disabled',true);
    }
    if (this.value.length == 0) {
      $('#saveButton').prop('disabled',true);
    }
  });

  //CONFIRMACION
  $('#inputConfirm').on('keyup', function() {
    pass = $('#inputPass').val();
    confirm = $(this).val();
    if (confirm != pass) {
      $('#saveButton').prop('disabled',true);
    } else {
      $('#saveButton').prop('disabled',false);
    }
    if (this.value.length == 0) {
      $('#saveButton').prop('disabled',true);
    }
  })

  function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
      return true;
    }
    else {
      return false;
    }
  }

  //BORRAR BOTON CON CONFIRMACION
  $('#deleteButton').on('click', function(event) {
    event.preventDefault();
    var box = '<div class="field">';
      box += '<h1>Confirmacion</h1>';
      box += '<label class="label is-size-7">Esta acción es irreversible</label>';
      box += '<div class="control">';
        box += '<input id="confirmButton" class="button is-danger" type="submit" value="Eliminar Cuenta">';
      box += '</div>';
    box += '</div>';
    modal(box);
    //CONFIRMACION
    $('#confirmButton').on('click', function(event) {
      event.preventDefault();
      var tie_key = localStorage.getItem("tie_key");
      var tie_user = localStorage.getItem("tie_user")
      $.ajax({
        type: 'POST',
        url: 'http://localhost/tiendero/www/php/perfil/perfil-borra.php',
        data: {
          auth : tie_key,
          user : tie_user
        },
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false
      })
      .done(function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            localStorage.removeItem("tie_key");
            localStorage.removeItem("tie_user");
            localStorage.removeItem("tie_log");
            window.location.href = 'index.html'
          } else {
            toast(value.message)
          }
        })
      })
      .fail(function() {
        console.log("error");
      })
    });
  });

});
