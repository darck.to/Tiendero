$(function() {
  //AUTOCOMPLETE OFF
  $('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  //PERFIL CONGIGURACION OBTIENE INFORMACION DE TIENDA
  cargaTienda();

  function cargaTienda(e) {
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user")
    $.ajax({
      type: 'POST',
      url: 'http://localhost/tiendero/www/php/tienda/tienda-carga.php',
      data: {
        auth : tie_key,
        user : tie_user
      },
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false
    })
    .done(function(data) {
      $.each(data, function (name, value) {
        if (value.success) {
          $('#inputName').val(value.nom);
          $('#inputRed1').val(value.re1);
          $('#inputRed2').val(value.re2);
          $('#inputRed3').val(value.re3);
          $('#inputTelephone').val(value.tel)
          $('#inputNiv').val(value.niv);
          $('#inputTie').val(value.tie);
          if (value.flag == 1) {
            $('#pauseButton').val('Suspender Tienda');
            $('#formStoreEdit :input').prop("disabled", false)
          } else {
            $('#formStoreEdit :input').prop("disabled", true);
            $('#pauseButton').val('Activar Tienda')
          }
        } else {
          toast(value.message)
        }
      })
    })
    .fail(function() {
      console.log("error");
    })
  }

  //EDITAMOS EL USUARIO
  $('#formStoreEdit').submit(function(event) {
    event.preventDefault();
    var formData = new FormData(document.getElementById("formStoreEdit"));
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user")
    formData.append('auth',tie_key);
    formData.append('user', tie_user);
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');
    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    request.done(function(data) {
      $.each(data, function (name, value) {
        if (value.success) {
          toast(value.message)
        } else {
          toast(value.message)
        }
      })
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      //$('#formProfileEdit').trigger('reset');
    })
  })

  //CAMBIO DE NOMBRE TAMBIEN HABILITA GUARDADO
  $(".validator").on("input", function() {
     $('#saveButton').prop('disabled',false);
  });

  //EMAIL
  $('#inputRed1').on('keyup', function() {
    email = $(this).val();
    if (validateEmail(email)) {
      $('#saveButton').prop('disabled',false);
    } else {
      $('#saveButton').prop('disabled',true);
    }
    if (this.value.length == 0) {
      $('#saveButton').prop('disabled',true);
    }
  });
  function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
      return true;
    }
    else {
      return false;
    }
  }

  //SUSPENDET BOTON CON CONFIRMACION
  $('#pauseButton').on('click', function(event) {
    event.preventDefault();
    var box = '<div class="field">';
      box += '<h1>Confirmacion</h1>';
      box += '<label class="label is-size-7">Esta acción es reversible</label>';
      box += '<div class="control">';
        box += '<input id="confirmButton" class="button is-danger" type="submit" value="' + $(this).val() +  '">';
      box += '</div>';
    box += '</div>';
    modal(box);
    //CONFIRMACION
    $('#confirmButton').on('click', function(event) {
      event.preventDefault();
      var tie_key = localStorage.getItem("tie_key");
      var tie_user = localStorage.getItem("tie_user");
      var tie  = $('#inputTie').val()
      $.ajax({
        type: 'POST',
        url: 'http://localhost/tiendero/www/php/tienda/tienda-suspende.php',
        data: {
          auth : tie_key,
          user : tie_user,
          tienda : tie
        },
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false
      })
      .done(function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            cargaTienda();
            xmodal()
          } else {
            cargaTienda();
            toast(value.message)
          }
        })
      })
      .fail(function() {
        console.log("error");
      })
    });
  });

});
