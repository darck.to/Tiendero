$(function() {
  //CARGAMOS EL NUMERO DE CATEGORIAS DISPONIBLES Y EL LISTADO
  cargaCategorias();

  function cargaCategorias(e) {
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user")
    $.ajax({
      type: 'POST',
      url: 'http://localhost/tiendero/www/php/categorias/categorias-carga.php',
      data: {
        auth : tie_key,
        user : tie_user
      },
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false
    })
    .done(function(data) {
      var content = '';
      $.each(data, function (name, value) {
        content += '<tr>'
        if (value.num) {
          $('#catNumbers').html(value.num)
        } else if (value.success) {
          content += '<td class="has-text-centered"><input type="checkbox" name="cat-' + value.id_cat + '"></td>';
          content += '<td class="has-text-centered categoria-edita" val="' + value.id_cat + '" nom="' + value.nom + '" pad="' + value.pad + '"><i class="far fa-edit handed"></i></td>';
          content += '<td>' + value.nom + '</td>';
          content += '<td>' + value.pad + '</td>';
          //SI ES ALL ES GENERAL
          var general = '<i class="fas fa-times"></i>';
          if (value.id_per) {
            general = '<i class="fas fa-check"></i>'
          }
          content += '<td class="has-text-centered">' + general + '</td>';
          content += '<td></td>'
          content += '<td class="has-text-centered categoria-borra" val="' + value.id_cat + '" "><i class="far fa-trash-alt handed"></i></td>'
        } else {
          content += '<td>No Dato</td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>';
          content += '<td></td>'
        }
        content += '</tr>';
      })
      $('#tablaContenidoCategorias').html(content);
      //EDITA CATEGORIA
      $('.categoria-edita').on('click', function(event) {
        ioCategorias(2,$(this).attr('val'),$(this).attr('nom'),$(this).attr('pad'))
      });
      //BORRA CATEGORIA
      $('.categoria-borra').on('click', function(event) {
        ioCategorias(3,$(this).attr('val'))
      });
    })
    .fail(function() {
      console.log("error");
    })
  }

  //CATEGORíA NUEVA
  $('.categoria-nuevo').on('click', function(event) {
    event.preventDefault();
    ioCategorias(0)
  });

  //FUNCTION CREA/MODIFICA CATEGORIAS
  function ioCategorias(io, ide, nome, pade) {
    //RECIBIMOS LA FUENTE PARA 0 O 1
    var id = (io === 0) ? "" : ide;
    var nom = (io === 0) ? "" : nome;
    var pad = (io === 0) ? "" : pade;
    var tagTitle = (io === 0) ? 'Nueva' : 'Edita';
    var tagSave = (io === 0) ? 'Guardar' : 'Edita';
    //INTERFAZ
    if (io === 3) {
      var box = '<h1>Borrar Categoria</h1>';
      box += '<div class="field">';
        box += '<div class="control">';
        box += '<label class="label">Esta acción es irreversible</label>';
          box += '<input id="confirmButton" class="button is-danger" type="submit" value="Confirmar">';
        box += '</div>';
      box += '</div>';
    } else {
      var box = '<h1>' + tagTitle + ' Categoria</h1>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="nameCategory" class="input" type="text" placeholder="Nombre de la Categoria" value="' + nom + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="fatherCategory" class="input" type="text" placeholder="Categoria Padre" value="' + pad + '">';
          box += '<input id="fatherId" type="hidden" value="' + id + '">';
        box += '</div>';
      box += '</div>';
      box += '<div class="field">';
        box += '<div class="control">';
          box += '<input id="confirmButton" class="button is-danger" type="submit" value="' + tagSave + '">';
        box += '</div>';
      box += '</div>';
    }
    modal(box);

    //GUARDA CATEGORIA
    $('#confirmButton').on('click', function(event) {
      event.preventDefault();
      var nombre = $('#nameCategory').val();
      var padre = $('#fatherCategory').val();
      var formData = new FormData();
      formData.append('nom', nombre);
      formData.append('fat', padre);
      formData.append('id', id);
      formData.append('io', io);
      formData.append('auth', localStorage.getItem("tie_key"));
      formData.append('user', localStorage.getItem("tie_user"))
      $.ajax({
        type: 'POST',
        url: 'http://localhost/tiendero/www/php/categorias/categorias-nueva.php',
        data: formData,
        processData: false,
        contentType: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false
      })
      .done(function(data) {
        $.each(data, function (name, value) {
          if (value.success) {
            toast(value.message);
            xmodal();
            cargaCategorias()
          } else {
            toast(value.message)
          }
        })
      })
      .fail(function() {
        console.log("error");
      })
    });
  }
});
