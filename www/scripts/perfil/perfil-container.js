$(function() {


  //ACCIONES DE MENU
  $('.menu-nav').on('click', function(event) {
    event.preventDefault();
    //IS-ACTIVE
    $('.menu-nav').removeClass('is-active');
    $(this).addClass('is-active');
    //NAVEGACION
    var destino = $(this).attr('href');
    $('#perfilSubContainer').html('');
    $('#perfilSubContainer').load('templates/perfil/' + destino + '.html')
  })

});
