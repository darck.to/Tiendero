$(function() {
  //HAY LOGIN?
  var tie_log = localStorage.getItem("tie_log");
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  switch(tie_log) {
    case "0":
      //NO LOGIN
      $('.user-name').html('Usuarios');
      $('.user-name').removeAttr('href');
      carga_menu_options(0);
      break;
    case null:
      //VARIABLE NO ESTABLECIDA
      $('.user-name').html('Usuarios');
      $('.user-name').removeAttr('href');
      carga_menu_options(0);
      break;
    case "1":
      //LOGIN LOCAL
      carga_menu_options(1);
      //CARGA EL CARRITO EN LAS OPCIONES DEL MENU
      cargaCarritoMenu();
      break;
  }

  $(".navbar-burger").click(function() {
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  })


  //FUNCTION CARGA OPCIONES DE MENU DE USUARIO
  function carga_menu_options(e) {
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    var opc = e;
    $.ajax({
      type: 'POST',
      url: 'http://localhost/tiendero/www/php/menu/menu-carga-options.php',
      data: {
        auth : tie_key,
        user : tie_user,
        opc : opc
      },
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var profile = '', buttons = '';
        $.each(data, function (name, value) {
          if (value.success == true) {
            if (value.level == 0) {
              profile += '<a class="navbar-item profile-nav ' + value.class + '" href="' + value.ref + '">' + value.txt + '</a>';
            }
            if (value.level == 9) {
              profile += '<hr class="' + value.class + '">';
            }
            if (value.level == 1) {
              buttons += '<a class="button profile-nav ' + value.class + '" href="' + value.ref + '">' + value.txt + '</a>';
            }
            if (value.level == 8) {
              buttons += '<a class="button profile-logout ' + value.class + '" href="' + value.ref + '">' + value.txt + '</a>';
            }
          }
        })
        $('#perfil-options').html(profile);
        $('#perfil-buttons').html(buttons);

        //NAVEGADOR DE PERFIL
        $('.profile-nav').on('click', function(event) {
          event.preventDefault();
          var dest = $(this).attr('href')
          $.ajax({
            type: 'POST',
            url: 'http://localhost/tiendero/www/php/perfil/perfil-inicia.php',
            data: {
              auth : tie_key,
              user : tie_user,
              dest : dest
            },
            async: true,
            dataType: 'json',
            crossDomain: true,
            context: document.body,
            cache: false
          })
          .done(function(data) {
            var destino = '';
            var blank = 0;
            $.each(data, function (name, value) {
              if (value.success) {
                blank = value.blank;
                destino = value.destino
              }
            })
            if (blank == 0) {
              modal();
              $('.modal-content').load(destino)
            } else {
              window.location.href = destino
            }
          })
          .fail(function() {
            console.log("error");
          })
        })

        //SESSION CLOSE
        $('.profile-logout').on('click', function(event){
          event.preventDefault();
          //CIERRA SESION PHP
          $.ajax({
            type: 'POST',
            url: 'http://localhost/tiendero/www/php/init/init-start.php',
            data: {
              a: 0
            },
            cache: false,
            success: function(data) {
              localStorage.removeItem("tie_key");
              localStorage.removeItem("tie_user");
              localStorage.removeItem("tie_log");
              window.location.href = '.'
            },
            error: function(xhr, tst, err) {
              toast(err)
            }
          })
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }

  function cargaCarritoMenu(e) {
    actualCarrito()
    .done(function(data) {
      var precio = 0;
      var total = 0;
      $.each(data, function (name, value) {
          if (value.success) {
            var content = ""
            $.each(data, function (key, cont) {
              if (cont.success) {
                $.each(cont.carrito, function (name, value) {
                  content += '<a class="navbar-link" href="carrito.html">';
                    content += '<span class="icon-text">';
                      content += '<span class="icon">';
                        content += '<i class="fas fa-shopping-cart"></i>';
                      content += '</span>';
                    content += '</span>';
                  content += '</a>'
                  content += '<div class="navbar-dropdown">';
                    var pu = 0;
                    $.each(value.productos, function (id, val) {
                      pu = ((parseFloat(val.precio) * val.cantidad) - (parseFloat(val.precio) * parseFloat(val.descuento)));
                      precio = (precio + parseFloat(pu));
                      total = (total + (parseFloat(val.precio) * val.cantidad))
                    });
                    content += '<div class="navbar-item">';
                      content += '<span><b>Tu Total:</b></span>';
                    content += '</div>';
                    content += '<div class="navbar-item">';
                      content += '<span>Precio con desc: ' + precio.toFixed(2) + '</span>';
                    content += '</div>';
                    content += '<div class="navbar-item">';
                      content += '<span><small>Precio sin desc: ' + total.toFixed(2) + '</small></span>';
                    content += '</div>';
                    content += '<hr class="navbar-divider">';
                    content += '<a class="navbar-item ir-carrito" href="carrito.html">';
                      content += 'Ir al carrito';
                    content += '</a>';
                  content += '</div>'
                })
              }
            })
            $('#menuCarro').html(content)

            //IR AL CARRITO ACTUAL
            $('.is-carrito').on('click', function(e) {
              
            })
          } else {
            box += '<div class="navbar-item">';
              box += '<span><b>Carrito Vacio</b></span>';
            box += '</div>';
            toast(value.message)
          }
      })
    })
    .fail(function() {
        toast('Error Producto')
    }) 
  }

})
