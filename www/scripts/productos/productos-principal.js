$(function() {
  //CARGA EL CONTENIDO EN DOM
  loadCarrouselProducts()
  .done(function(data) {
    var content = '<div class="columns">';
    var divOpCol4 = '<div class="column is-2">';
    var divCl = '</div>'
    $.each(data, function (name, value) {
      if (value.success && value.flag) {
        content += divOpCol4 + '<div class="box">';
          content += '<a href="producto.php?a=' + value.pro + '">';
            content += '<h1 class="title is-size-5">' + value.nom + '</h1>';
            content += '<span></span>';
          content += '</a>';
        content += '</div>' + divCl
      } else if (value.num) {
        $('#productosCount').html(value.num)
      } else {
        toast(value.message)
      }
    })
    content += '</div>';
    $('#productosBannerPrincipal').html(content)
  })
  .fail(function() {
    toast('Error Productos')
  })

  function loadCarrouselProducts(e) {
    var form_data = new FormData();
    var form_method = 'post';
    var form_url = 'php/productos/productos-banner-principal.php';
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    form_data.append('auth',tie_key);
    form_data.append('user', tie_user)
    return $.ajax({
      url: form_url,
      method: form_method,
      data: form_data,
      contentType: false,
      processData: false,
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    })
  }
});