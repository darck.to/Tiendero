$(function() {
    //RECIBIMOS LOS DATOS POR GET MEDIANTE VARIABLES Y LA MANDAMOS LLAMAR
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=')
            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1])
            }
        }
        return false
    }

    cargaProducto(getUrlParameter('a'))
    .done(function(data) {
        var content = '<div class="columns is-multiline">';
        var divOpCol12 = '<div class="column is-12">';
        var divOpCol2 = '<div class="column is-2">';
        var divCl = '</div>'
        $.each(data, function (name, value) {
            //DATOS DE LA CONSULTA
            if (value.success && value.flag === 'product') {
                //TITLE
                $('title').html(value.nom + '&nbsp;en&nbsp;Tiendero');
                //DATOS DEL PRODUCTO
                content += divOpCol12 + '<div class="box">';
                    content += '<h1 class="title is-size-5">' + value.nom + '</h1>';
                    content += '<p><small>Precio $' + value.pre + '</small></p>';
                    content += '<p><small>en inventario: <span class="en-inventario">' + value.can + '</span></small></p>';
                    content += '<p><small>Categoria ' + value.cat + '</small></p>';
                    content += '<div class="field"><label class="label">Cantidad</label><input id="proCant" class="input para-agregar" value=1></input></div>',
                    content += '<span class="handed agrega-carrito" tie="' + value.tie + '" pro="' + value.pro + '"><i class="fas fa-2x fa-shopping-cart p-2"></i></span>',
                content += '</div>' + divCl
                //DATOS DE LA TIENDA
                content += divOpCol2 + '<div class="box">';
                    content += '<a href="tienda.php?a=' + value.tie + '">';
                        content += '<h1 class="title is-size-5">' + value.not + '</h1>';
                    content += '</a>';
                    content += '<p>' + value.re1 + '</p>';
                    content += '<p>' + value.re2 + '</p>';
                    content += '<p>' + value.re3 + '</p>';
                    content += '<p>' + value.tel + '</p>';
                content += '</div>' + divCl
            }
            if (value.success === false) {
                toast(value.message)
            }
        })
        content += '</div>';
        $('#productoShow').html(content);
        //NO ES POSIBLE AGREGAR MAS DE LO EXISTENTE EN INVENTARIO
        $('.para-agregar').on('change keyup blur', function(e) {
            var enInv = $('.en-inventario').html();
            if ($(this).val() > parseInt(enInv)) {
                e.preventDefault();
                toast('No es posible agregar mas de lo existente en inventario');
                $(this).val(enInv)
            } else if ($(this).val() <= 0) {
                e.preventDefault();
                toast('No es posible agregar 0');
                $(this).val(enInv)
            }
        })
        //AGREGA AL CARRITO
        $('.agrega-carrito').on('click', function(e) {
            var tienda = $(this).attr('tie');
            var producto = $(this).attr('pro');
            var cantidad = parseInt($('#proCant').val())
            agregaCarrito(tienda, producto, cantidad)
            .done(function(data) {
                $.each(data, function (name, value) {
                    if (value.success) {
                        toast(value.message)
                    } else {
                        toast(value.message)
                    }
                })
            })
            .fail(function() {
                toast('Error Producto')
            })
        })
    })
    .fail(function() {
        toast('Error Producto')
    })

    //CARGA INFORMACION DE TIENDA E INVENTARIO EN VENTA
    function cargaProducto(e) {
        var id = e;
        var form_data = new FormData();
        var form_method = 'post';
        var form_url = 'php/productos/productos-carga-individual.php';
        var tie_key = localStorage.getItem("tie_key");
        var tie_user = localStorage.getItem("tie_user");
        form_data.append('auth',tie_key);
        form_data.append('user', tie_user);
        form_data.append('id', id)
        return $.ajax({
            url: form_url,
            method: form_method,
            data: form_data,
            contentType: false,
            processData: false,
            async: true,
            dataType: 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
        })
    }
})