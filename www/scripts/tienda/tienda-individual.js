$(function() {
    //RECIBIMOS LOS DATOS POR GET MEDIANTE VARIABLES Y LA MANDAMOS LLAMAR
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=')
            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1])
            }
        }
        return false
    }

    cargaTienda(getUrlParameter('a'))
    .done(function(data) {
        var content = '<div class="columns is-multiline">';
        var divOpCol12 = '<div class="column is-12">';
        var divOpCol2 = '<div class="column is-2">';
        var divCl = '</div>'
        $.each(data, function (name, value) {
            //DATOS DE LA TIENDA
            if (value.success && value.flag === 'store') {
                //TITLE
                $('title').html(value.nom + '&nbsp;en&nbsp;Tiendero');
                content += divOpCol12 + '<h1 class="title is-size-5">' + value.nom + '</h1>';
                    content += '<p>' + value.re1 + '</p>';
                    content += '<p>' + value.re2 + '</p>';
                    content += '<p>' + value.re3 + '</p>';
                    content += '<p>' + value.tel + '</p>';
                content += divCl
            }
            //NUMERO DE PRODUCTOS
            if (value.num) {
                content += divOpCol12 + '<span class="is-title">Productos entontrados: ' + value.num + '</p>' + divCl;
            }
            //DATOS DE LOS PRODUCTOS
            if (value.success && value.flag === 'product') {
                content += divOpCol2 + '<div class="box">';
                    content += '<a href="producto.php?a=' + value.pro + '">';
                        content += '<h1 class="title is-size-5">' + value.nom + '</h1>';
                    content += '</a>';
                    content += '<p><small>Precio $' + value.pre + '</small></p>';
                    content += '<p><small>en inventario: ' + value.can + '</small></p>';
                    content += '<p><small>Categoria ' + value.cat + '</small></p>';
                content += '</div>' + divCl
            }
            if (value.success === false) {
                toast(value.message)
            }
        })
        content += '</div>';
        $('#tiendaShow').html(content)
    })
    .fail(function() {
        toast('Error Tienda')
    })

    //CARGA INFORMACION DE TIENDA E INVENTARIO EN VENTA
    function cargaTienda(e) {
        var id = e;
        var form_data = new FormData();
        var form_method = 'post';
        var form_url = 'php/tienda/tienda-carga-individual.php';
        var tie_key = localStorage.getItem("tie_key");
        var tie_user = localStorage.getItem("tie_user");
        form_data.append('auth',tie_key);
        form_data.append('user', tie_user);
        form_data.append('id', id)
        return $.ajax({
            url: form_url,
            method: form_method,
            data: form_data,
            contentType: false,
            processData: false,
            async: true,
            dataType: 'json',
            crossDomain: true,
            context: document.body,
            cache: false,
        })
    }
})