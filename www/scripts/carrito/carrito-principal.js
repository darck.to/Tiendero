$(function() {
  //CARGA EL CARRITO ACTUAL SI EXISTE LOGIN
  cargaCarrito()

  function cargaCarrito(e) {
    actualCarrito()
    .done(function(data) {
      var carrito;
      var precio = 0;
      var total = 0;
      var num = 0;
      var content = '';
      $.each(data, function (name, value) {
        if (value.success) {
          $.each(data, function (key, cont) {
            if (cont.success) {
              $.each(cont.carrito, function (name, value) {
                var pu = 0;
                $.each(value.productos, function (id, val) {
                  //CARRITO
                  carrito = value.id;
                  content += '<tr>';
                    pu = ((parseFloat(val.precio) * val.cantidad) - (parseFloat(val.precio) * parseFloat(val.descuento)));
                    precio = (precio + parseFloat(pu));
                    total = (total + (parseFloat(val.precio) * val.cantidad));
                    num = parseInt(num) + parseInt(val.cantidad);
                    content += '<td class="has-text-centered"><input type="checkbox" name="pro_' + val.transaccion + '"></td>';
                    content += '<td>' + val.tienda + '</td>';
                    content += '<td>' + val.producto + '</td>';
                    content += '<td>';
                      content += '<div class="columns">';
                        content += '<div class="column is-half">';
                          content += '<input type="text" class="input carrito_input_modifica" car="' + value.id + '" tra="' + val.transaccion + '" value="' + val.cantidad + '">';
                        content += '</div>';
                        content += '<div class="column is-half">';
                          content += '<span class="num_' + val.transaccion + '">0 en inventario' + inventarioTienda(value.id,val.transaccion) + '</span>';
                        content += '</div>';
                      content += '</div>';
                    content += '</td>';
                    content += '<td>' + parseFloat(val.precio) + '</td>';
                    content += '<td>' + (val.descuento * 100) + '%</td>';
                    content += '<td>' + pu + '</td>';
                    content += '<td class="has-text-centered producto-borra" val="' + val.transaccion + '"><i class="far fa-trash-alt handed"></i></td>';
                  content += '</tr>'
                })
              })
            }
          })
          $('#tablaContenidoCarrito').html(content)
          content += '<tfoot>';
            content += '<tr>';
              content += '<th></th>';
              content += '<th></th>';
              content += '<th>Tu Total:</th>';
              content += '<th>' + num + '</th>';
              content += '<th>' + total.toFixed(2) + '</th>';
              content += '<th></th>';
              content += '<th>' + precio.toFixed(2) + '</th>';
              content += '<th></th>';
            content += '</tr>';
          content += '</tfoot>';
        $('#tablaContenidoCarrito').append(content)
        } else {
          content += '<tr>';
            content += '<td>No Dato</td>';
            content += '<td></td>';
            content += '<td></td>';
            content += '<td></td>';
            content += '<td></td>';
            content += '<td></td>';
            content += '<td></td>';
            content += '<td></td>';
          content += '</tr>';
          toast(value.message)
        }
      })
      
      //CAMBIA CANTIDADES EN BASE A LAS EXISTENCIAS
      $('.carrito_input_modifica').on('keyup', function(e) {
        var car = $(this).attr('car');
        var tra = $(this).attr('tra');
        var can = $(this).val();
        var esto = $(this);
        revisaSiMayorCarrito(car,tra,can)
        .done(function(data) {
          $.each(data, function (name, value) {
            //REVISA QUIEN ESE MAYOR SI ES POSITIVA LA BUSQUEDA
            if (value.success) {
              if (value.flag) {
                toast('No es posible agregar mas de lo existente en inventario');
                esto.val(value.mayor)
              }
            } else {
              toast(value.message)
            }
          })
        })
        .fail(function() {
          toast('Error Producto')
        })
      })
  
      //REVISA EL INVENTARIO EN EL CARRITO
      function inventarioTienda(car,tra) {
        var carrito = car;
        var transaccion = tra
        revisaInventarioCarrito(carrito,transaccion)
        .done(function(data) {
          $.each(data, function (name, value) {
            //REVISA EL INVENTARIO
            if (value.success) {
              $('.num_' + transaccion).html(value.inventario + ' en inventario')
            } else {
              toast(value.message)
            }
          })
        })
        .fail(function() {
          toast('Error Producto')
        })
      }
  
      //CUPONERA
      $('#cuponInput').html('<input class="input cupon-input" type="text" placeholder="Cupon"></input>');
      $('#cuponButton').html('<input id="cargaCupon" class="button is-primary" type="submit" value="Agregar Cupon" car="' + carrito + '">');
      //PAGAR COMPRAR BUTTON
      $('#comprarButton').html('<input class="button is-link" type="submit" value="Comprar">');
  
      //agrega cupon input
      $('#cargaCupon').on('click', function(e) {
        var cupon = $('.cupon-input').val();
        var carrito = $(this).attr('car');
        var stringaUth_key = localStorage.getItem("tie_key");
        var stringaUth_user = localStorage.getItem("tie_user");
        var form_data = new FormData();
        form_data.append('auth',stringaUth_key);
        form_data.append('user', stringaUth_user);
        form_data.append('carrito', carrito);
        form_data.append('cupon', cupon);
        $.ajax({
          type: 'POST',
          url: 'php/carrito/carrito-cupon.php',
          data: form_data,
          cache: false,
          contentType: false,
          processData: false,
          async: false,
          success: function(data) {
            $.each(data, function (name, value) {
              if (value.success) {
                toast(value.message);
                cargaCarrito()
              } else {
                toast(value.message);
              }
            })
          },
          error: function(xhr, tst, err) {
            toast(err)
          }
        })
      })
  
    })
    .fail(function() {
        toast('Error Producto')
    })
  }

})