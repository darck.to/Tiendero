$(function() {

  //SEGUNDO CARGAMOS EL TEMPLATE DEL CARRUSEL DE TIENDAS
  loadPrincipalStores();
  //TERCERO CARGAMOS EL TEMPLATE DE PRODUCTOS DESTACADOS
  loadPrincipalProducts();

  //CARGA PRINCIPAL TIENDA
  function loadPrincipalStores(e) {
    $('#principalStores').load('templates/tienda/tienda-principal.html')
  }

  //CARGA PRINCIPAL PRODUCTOS
  function loadPrincipalProducts(e) {
    $('#principalProducts').load('templates/productos/productos-principal.html')
  }
});
