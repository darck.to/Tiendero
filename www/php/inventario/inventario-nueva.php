<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $nom = mysqli_real_escape_string($mysqli,$_POST['nom']);
  $cat = mysqli_real_escape_string($mysqli,$_POST['cat']);
  $pre = mysqli_real_escape_string($mysqli,$_POST['pre']);
  $can = mysqli_real_escape_string($mysqli,$_POST['can']);
  $ord = mysqli_real_escape_string($mysqli,$_POST['ord']);
  $ped = mysqli_real_escape_string($mysqli,$_POST['ped']);

  $id_pro = generateRandomString(8);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    //ID DEL PERFIL
    $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      //SI ES EDICION/GUARDADO/BORRADO
      if ($_POST['io'] == 2) {
        $id = mysqli_real_escape_string($mysqli,$_POST['id']);
        $consulta = "UPDATE inve_table SET nom = '".$nom."', id_cat = '".$cat."', pre = '".$pre."', can = '".$can."', ord = '".$ord."', ped = '".$ped."' WHERE id_pro = '".$id."' ";
        $tag = "Editada";
      } elseif ($_POST['io'] == 0) {
        //ID DE LA TIENDA
        $sqlt = $mysqli->query("SELECT id_tie FROM tien_table WHERE id_per = '".$rowp['id_per']."'");
        if ($sqlt->num_rows > 0) {
            $rowt = $sqlt->fetch_assoc();
          $consulta = "INSERT INTO inve_table (id_per, id_pro, id_cat, fec, id_tie, nom, pre, can, ord, ped) VALUES ('".$rowp['id_per']."', '".$id_pro."', '".$cat."', '".$fechaActual."', '".$rowt['id_tie']."', '".$nom."', ".$pre.", ".$can.", ".$ord.", ".$ped.")";
          $tag = "Guardada";
        }
      } elseif ($_POST['io'] == 3) {
        $id = mysqli_real_escape_string($mysqli,$_POST['id']);
        $consulta = "DELETE FROM inve_table WHERE id_pro = '".$id."' ";
        $tag = "Eliminada";
      }
      $sqlc = $mysqli->query($consulta);
      if ($sqlc) {
        $resultados[] = array("success"=> true, "message"=> $sqlc = $mysqli->affected_rows . " Inventario " . $tag . "/". $nom . " " . mysqli_error($mysqli));
      } else {
        $resultados[] = array("success"=> false, "message"=> "No se pudo agregar " . mysqli_error($mysqli));
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No id perfil");
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
  }

  print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
