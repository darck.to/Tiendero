<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    //ID DEL PERFIL
    $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      $sqli = $mysqli->query("SELECT nom, id_cat, can, ord, ped, pre, id_pro, id_tie FROM inve_table WHERE id_per = '".$rowp['id_per']."'");
      if ($sqli->num_rows > 0) {
        $n = 0;
        while ($rowi = $sqli->fetch_assoc()) {
          $resultados[] = array("success"=> true, "nom"=> $rowi['nom'], "can"=> $rowi['can'], "ord"=> $rowi['ord'], "ped"=> $rowi['ped'], "pre"=> $rowi['pre'], "cat"=> $rowi['id_cat'], "pro"=> $rowi['id_pro'], "tie"=> $rowi['id_tie']);
          $n++;
        }
        $resultados[] = array("num"=> $n);
      } else {
        $resultados[] = array("success"=> false, "message"=> "No categorias");
        $resultados[] = array("num"=> "0");
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No id de perfil");
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
