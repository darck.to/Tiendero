<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $nom = mysqli_real_escape_string($mysqli,$_POST['nom']);
  $fat = mysqli_real_escape_string($mysqli,$_POST['fat']);

  $id_cat = generateRandomString(8);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    //ID DEL PERFIL
    $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      //SI ES EDICION/GUARDADO/BORRADO
      if ($_POST['io'] == 2) {
        $id = mysqli_real_escape_string($mysqli,$_POST['id']);
        $consulta = "UPDATE cate_table SET nom = '".$nom."', pad = '".$fat."' WHERE id_cat = '".$id."' ";
        $tag = "Editada";
      } elseif ($_POST['io'] == 0) {
        $consulta = "INSERT INTO cate_table (nom, pad, id_cat, id_per) VALUES ('".$nom."', '".$fat."', '".$id_cat."', '".$rowp['id_per']."')";
        $tag = "Guardada";
      } elseif ($_POST['io'] == 3) {
        $id = mysqli_real_escape_string($mysqli,$_POST['id']);
        $consulta = "DELETE FROM cate_table WHERE id_cat = '".$id."' ";
        $tag = "Eliminada";
      }
      $sqlc = $mysqli->query($consulta);
      if ($sqlc) {
        $resultados[] = array("success"=> true, "message"=> $sqlc = $mysqli->affected_rows . " Categoria " . $tag . "/". $nom);
      } else {
        $resultados[] = array("success"=> false, "message"=> "No se pudo agregar");
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No id perfil");
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
