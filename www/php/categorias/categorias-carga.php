<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    //ID DEL PERFIL
    $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      $sqlc = $mysqli->query("SELECT nom, pad, id_cat, id_per FROM cate_table WHERE id_per = '".$rowp['id_per']."' OR id_per = 'ALL' ");
      if ($sqlc->num_rows > 0) {
        $n = 0;
        while ($rowc = $sqlc->fetch_assoc()) {
          ($rowc['id_per'] === "ALL") ? $general = true : $general = false;
          $resultados[] = array("success"=> true, "nom"=> $rowc['nom'], "pad"=> $rowc['pad'], "id_cat"=> $rowc['id_cat'], "id_per"=> $general);
          $n++;
        }
        $resultados[] = array("num"=> $n);
      } else {
        $resultados[] = array("success"=> false, "message"=> "No categorias");
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No id de perfil");
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
