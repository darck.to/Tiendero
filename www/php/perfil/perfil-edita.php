<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  //SI EXISTE LA VARIABLE DE SESSION
  if (isset($_SESSION['log'])) {
    include_once('../../functions/abre_conexion.php');

  	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $nombre = mysqli_real_escape_string($mysqli,$_POST['nombre']);
    $primer = mysqli_real_escape_string($mysqli,$_POST['primer']);
    $segundo = mysqli_real_escape_string($mysqli,$_POST['segundo']);
    $usuario = mysqli_real_escape_string($mysqli,$_POST['usuario']);
    $correo = mysqli_real_escape_string($mysqli,$_POST['correo']);
    $telefono = mysqli_real_escape_string($mysqli,$_POST['telefono']);
    $contrasena = mysqli_real_escape_string($mysqli,$_POST['contrasena']);

    $sql = $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      //PRIMERO USER NAME
      if ($usuario != $row['nom']) {
        $sqlu = $mysqli->query("UPDATE auth_table SET nom = '".$usuario."' WHERE id_usr = '".$row['id_usr']."'");
        if ($sqlu) {
          $resultados[] = array("success"=> true, "message"=> "Usuario Actualizado");
        } else {
          $resultados[] = array("success"=> true, "message"=> "Error al actualizar el usuario" . mysqli_error($mysqli));
        }
      }
      //ENTONCES CONTRASENA
      if (strlen(trim($contrasena)) > 0) {
        $usuario_clave = password_hash($contrasena, PASSWORD_BCRYPT); // encriptamos la contraseña ingresada con md5 ;)
        $sqlc = $mysqli->query("UPDATE auth_table SET pas = '".$usuario_clave."' WHERE id_usr = '".$row['id_usr']."'");
        if ($sqlc) {
          $resultados[] = array("success"=> true, "message"=> "Contrasena Actualizado");
        } else {
          $resultados[] = array("success"=> true, "message"=> "Error al actualizar la contrasena" . mysqli_error($mysqli));
        }
      }
      $sqlp = $mysqli->query("UPDATE perf_table SET nom = '".$nombre."', ape = '".$primer."', apm = '".$segundo."', mai = '".$correo."', tel = '".$telefono."' WHERE id_usr = '".$row['id_usr']."'");
      if ($sqlp) {
        $resultados[] = array("success"=> true, "message"=> "Perfil Actualizado");
      } else {
        $resultados[] = array("success"=> true, "message"=> "Error al actualizar el perfil" . mysqli_error($mysqli));
      }
    } else {
      $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Error, contact support " . mysqli_error($mysqli));
    }

    include_once('../../functions/cierra_conexion.php');
  } else {
    $resultados[] = array("success"=> false, "type"=> "profile edit", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No session");
  }
	print json_encode($resultados);
?>
