<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  //SI EXISTE LA VARIABLE DE SESSION
  if (isset($_SESSION['log'])) {
    include_once('../../functions/abre_conexion.php');

  	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
      if ($sqlp->num_rows > 0) {
        $rowp = $sqlp->fetch_assoc();
        //BORRA TIENDA
        $sqlt = $mysqli->query("DELETE FROM tien_table WHERE id_per = '".$rowp['id_per']."'");
        if ($sqlt) {
          //BORRA PERFIL
          $sqlu = $mysqli->query("DELETE FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
          if ($sqlu) {
            //BORRA USUARIO
            $sqla = $mysqli->query("DELETE FROM auth_table WHERE id_usr = '".$row['id_usr']."'");
            if ($sqla) {
              $resultados[] = array("success"=> true, "message"=> "Usuario Eliminado");
            } else {
              $resultados[] = array("success"=> true, "message"=> "Error al eliminar el usuario" . mysqli_error($mysqli));
            }
            $resultados[] = array("success"=> true, "message"=> "Perfil Eliminado");
          } else {
            $resultados[] = array("success"=> true, "message"=> "Error al eliminar el perfil" . mysqli_error($mysqli));
          }
          $resultados[] = array("success"=> true, "message"=> "Tienda Eliminada");
        } else {
          $resultados[] = array("success"=> true, "message"=> "Error al eliminar tienda" . mysqli_error($mysqli));
        }
      } else {
        $resultados[] = array("success"=> false, "type"=> "profile delete", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Error, contact support " . mysqli_error($mysqli));
      }
    }
    include_once('../../functions/cierra_conexion.php');
  } else {
    $resultados[] = array("success"=> false, "type"=> "profile delete", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No session");
  }
	print json_encode($resultados);
?>
