<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $dest = mysqli_real_escape_string($mysqli,$_POST['dest']);

  switch ($dest) {
    case 'profile':
      //VER PERFIL
      $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
      if ($sql->num_rows > 0) {
        $resultados[] = array("success"=> true, "blank"=> 1, "destino"=> "perfil.html");
      } else {
        $resultados[] = array("success"=> true, "blank"=> 0, "destino"=> "templates/init/init-login.html");
      }
      break;
    case 'signin':
      //REGISTRARSE
      $resultados[] = array("success"=> true, "blank"=> 0, "destino"=> "templates/init/init-signin.html");
      break;
    case 'login':
      //INICIA SESION
      $resultados[] = array("success"=> true, "blank"=> 0, "destino"=> "templates/init/init-login.html");
      break;
    case 'store':
      //TIENDA
      $resultados[] = array("success"=> true, "blank"=> 1, "destino"=> "tienda.html");
      break;
    case 'products':
      //PRODUCTOS
      $resultados[] = array("success"=> true, "blank"=> 1, "destino"=> "productos.html");
      break;
    default:
      //SI FALLA ALGO
      $resultados[] = array("success"=> false, "message"=> "Algo falló");
      break;
  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
