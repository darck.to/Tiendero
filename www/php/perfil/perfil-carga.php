<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  //SI EXISTE LA VARIABLE DE SESSION
  if (isset($_SESSION['log'])) {
    include_once('../../functions/abre_conexion.php');

  	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $sql = $mysqli->query("SELECT id_usr, nom FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $sqlp = $mysqli->query("SELECT nom, ape, apm, mai, tel FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
      if ($sqlp->num_rows > 0) {
        $rowp = $sqlp->fetch_assoc();
        $resultados[] = array("success"=> true, "usr"=> $row['nom'], "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "mai"=> $rowp['mai'], "tel"=> $rowp['tel']);
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No sesion");
    }
    include_once('../../functions/cierra_conexion.php');
  } else {
    $resultados[] = array("success"=> false, "type"=> "profile load", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No session");
  }
	print json_encode($resultados);
?>
