<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  
  include_once('../../functions/abre_conexion.php');

  //CARGAMOS LOS PRODUCTOS
  $sql = $mysqli->query("SELECT `inve_table`.`nom` as nom, `inve_table`.`id_cat` as id_cat, `inve_table`.`can` as can, `inve_table`.`ord` as ord, `inve_table`.`ped` as ped, `inve_table`.`pre` as pre, `inve_table`.`id_pro` as id_pro, `inve_table`.`id_tie` as id_tie FROM `inve_table` LEFT JOIN `tien_table` ON `inve_table`.`id_tie` = `tien_table`.`id_tie` WHERE `tien_table`.`fla` = true");
  if ($sql->num_rows > 0) {
    $n = 0;
    while ($row = $sql->fetch_assoc()) {
      $resultados[] = array("success"=> true, "flag"=> true, "nom"=> $row['nom'], "can"=> $row['can'], "ord"=> $row['ord'], "ped"=> $row['ped'], "pre"=> $row['pre'], "cat"=> $row['id_cat'], "pro"=> $row['id_pro'], "tie"=> $row['id_tie']);
      $n++;
    }
    $resultados[] = array("num"=> $n);
  } else {
    $resultados[] = array("success"=> false, "message"=> "No productos");
    $resultados[] = array("num"=> "0");
  }

  //DE HABER USUARIO LO RECONOCEMOS
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $resultados[] = array("success"=> true, "flag"=> false, "message"=> "Si sesion");
  } else {
    $resultados[] = array("success"=> false, "message"=> "No sesion");
  }
  include_once('../../functions/cierra_conexion.php');
  
	print json_encode($resultados);
?>
