<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  if (empty($_POST['nom']) || empty($_POST['pas'])) {
    echo "El usuario o la contraseña no han sido ingresados correctamente!";
  } else {
    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $usuario_nombre = mysqli_real_escape_string($mysqli,$_POST['nom']);
    $usuario_clave = mysqli_real_escape_string($mysqli,$_POST['pas']);

    // comprobamos que los datos ingresados en el formulario coincidan con los de la BD
    $sqlogin = $mysqli->query("SELECT init_index, nom, pas, id_usr FROM auth_table WHERE nom = '".$usuario_nombre."'");
    if ($sqlogin->num_rows > 0) {
      $row = $sqlogin->fetch_assoc();
      $validPassword = password_verify($usuario_clave, $row['pas']);
      if ($validPassword){
        $auth_nombre = $row["nom"];
        $auth_number = $row['init_index'];
        //comprobamos la estructura de directorios de acuerdo a los datos del usuario
        $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
        if ($sqlp->num_rows > 0) {
          $rowp = $sqlp->fetch_assoc();
          $sqlt = $mysqli->query("SELECT id_tie FROM tien_table WHERE id_per = '".$rowp['id_per']."'");
          if ($sqlt->num_rows > 0) {
            $rowt = $sqlt->fetch_assoc();
            if(carrito_crea_estrutura($rowp['id_per'],$rowt['id_tie'])) {
              $resultados[] = array("success"=> true, "type"=>"login", "ip"=> $localIP, "date"=> $fechaActual, "det_key"=> $auth_number, "det_user"=> $auth_nombre);
            }
          }
        }
      } else {
        $resultados[] = array("success"=> false, "type"=>"login", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, no login ");
      }
    } else {
      $resultados[] = array("success"=> false, "type"=>"login", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, contact support");
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
