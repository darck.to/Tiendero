<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sqlp = $mysqli->query("SELECT type FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      $tipo = $rowp['type'];
      if ($tipo == 1) {
        $menu[] = array("main"=> "Administrador", "level"=> "Admin", "url"=> "adm/templates/config/config-users.html");
        $resultados[] = array("success"=> true, "message"=> "Login Admin", "type"=> 1, "menu"=> $menu);
      } elseif ($tipo == 2) {
        $menu[] = array("main"=> "Supervisor", "level"=> "Supervisor", "url"=> "adm/templates/config/config-users.html");
        $resultados[] = array("success"=> true, "message"=> "Login Supervisor", "type"=> 2, "menu"=> $menu);
      } elseif ($tipo == 3) {
        $menu[] = array("main"=> "Capturista", "level"=> "Capture", "url"=> "adm/templates/config/config-users.html");
        $resultados[] = array("success"=> true, "message"=> "Login Capturista", "type"=> 3, "menu"=> $menu);
      } else {
        $resultados[] = array("success"=> true, "message"=> "No tipo usuario", "type"=> false);
      }
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "Error de login");
  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
