<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $auth = mysqli_real_escape_string($mysqli, $_POST['auth']);
  $user = mysqli_real_escape_string($mysqli, $_POST['user']);

  $tienda_index = generateRandomString(8);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
    if ($sqlp->num_rows > 0) {
      $rowp = $sqlp->fetch_assoc();
      //CREA UNA TIENDA PARA EL USUARIO NUEVO Y SU ESTRUCTURA PARA EL CARRITO
      if (carrito_crea_estrutura($rowp['id_per'],$tienda_index)) {
        if ($sqlt = $mysqli->query("INSERT INTO tien_table (nom, niv, fec, id_per, id_tie) VALUES ('', 0, '".$fechaActual."', '".$rowp['id_per']."', '".$tienda_index."')")) {
          //RETROALIMENTACIÓN
          $resultados[] = array("success"=> true, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "tie_key"=> $auth, "tie_user"=> $user);
        } else {
          $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, contact support ".mysqli_error($mysqli));
        }
      }
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
