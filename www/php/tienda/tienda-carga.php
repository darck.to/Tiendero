<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  //SI EXISTE LA VARIABLE DE SESSION
  if (isset($_SESSION['log'])) {
    include_once('../../functions/abre_conexion.php');

  	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $sqlp = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
      if ($sqlp->num_rows > 0) {
        $rowp = $sqlp->fetch_assoc();
        $sqlp = $mysqli->query("SELECT nom, re1, re2, re3, tel, niv, id_tie, fla FROM tien_table WHERE id_per = '".$rowp['id_per']."'");
        if ($sqlp->num_rows > 0) {
          $rowp = $sqlp->fetch_assoc();
          $resultados[] = array("success"=> true, "nom"=> $rowp['nom'], "re1"=> $rowp['re1'], "re2"=> $rowp['re2'], "re3"=> $rowp['re3'], "tel"=> $rowp['tel'], "niv"=> $rowp['niv'], "tie"=> $rowp['id_tie'], "flag"=> $rowp['fla']);
        } else {
          $resultados[] = array("success"=> false, "message"=> "No tienda");
        }
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No sesion");
    }
    include_once('../../functions/cierra_conexion.php');
  } else {
    $resultados[] = array("success"=> false, "type"=> "profile load", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No session");
  }
	print json_encode($resultados);
?>
