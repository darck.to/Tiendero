<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  
  include_once('../../functions/abre_conexion.php');
  
  $id = mysqli_real_escape_string($mysqli,$_POST['id']);

  //CARGAMOS LAS TIENDAS
  $sqlt = $mysqli->query("SELECT nom, re1, re2, re3, tel, niv, id_tie FROM tien_table WHERE id_tie = '".$id."' AND fla = 1");
  if ($sqlt->num_rows > 0) {
    //INFORMACION DE LA TIENDA
    $rowt = $sqlt->fetch_assoc();
    $resultados[] = array("success"=> true, "flag"=> 'store', "nom"=> $rowt['nom'], "re1"=> $rowt['re1'], "re2"=> $rowt['re2'], "re3"=> $rowt['re3'], "tel"=> $rowt['tel'], "niv"=> $rowt['niv']);
    //CARGAMOS LOS PRODUCTOS DE LA TIENDA
    $sqlp = $mysqli->query("SELECT nom, id_cat, can, ord, ped, pre, id_pro FROM inve_table WHERE id_tie = '".$rowt['id_tie']."'");
    if ($sqlp->num_rows > 0) {
      $n = 0;
      while ($rowi = $sqlp->fetch_assoc()) {
        $resultados[] = array("success"=> true, "flag"=> 'product', "nom"=> $rowi['nom'], "can"=> $rowi['can'], "ord"=> $rowi['ord'], "ped"=> $rowi['ped'], "pre"=> $rowi['pre'], "cat"=> $rowi['id_cat'], "pro"=> $rowi['id_pro']);
        $n++;
      }
      $resultados[] = array("num"=> $n);
    } else {
      $resultados[] = array("success"=> false, "message"=> "No productos");
      $resultados[] = array("num"=> "0");
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "No tiendas");
  }

  //DE HABER USUARIO LO RECONOCEMOS
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $resultados[] = array("success"=> true, "flag"=> false, "message"=> "Si sesion");
  } else {
    $resultados[] = array("success"=> false, "message"=> "No sesion");
  }
  include_once('../../functions/cierra_conexion.php');
  
	print json_encode($resultados);
?>
