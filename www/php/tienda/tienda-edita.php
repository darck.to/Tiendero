<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  //SI EXISTE LA VARIABLE DE SESSION
  if (isset($_SESSION['log'])) {
    include_once('../../functions/abre_conexion.php');

  	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $nombre = mysqli_real_escape_string($mysqli,$_POST['nombre']);
    $red1 = mysqli_real_escape_string($mysqli,$_POST['red1']);
    $red2 = mysqli_real_escape_string($mysqli,$_POST['red2']);
    $red3 = mysqli_real_escape_string($mysqli,$_POST['red3']);
    $telefono = mysqli_real_escape_string($mysqli,$_POST['telefono']);
    $nivel = mysqli_real_escape_string($mysqli,$_POST['nivel']);
    $tienda = mysqli_real_escape_string($mysqli,$_POST['tienda']);

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $sqlp = $mysqli->query("UPDATE tien_table SET nom = '".$nombre."', re1 = '".$red1."', re2 = '".$red2."', re3 = '".$red3."', tel = '".$telefono."', niv = $nivel WHERE id_tie = '".$tienda."'");
      if ($sqlp) {
        $resultados[] = array("success"=> true, "message"=> "Tienda Actualizada");
      } else {
        $resultados[] = array("success"=> true, "message"=> "Error al actualizar tienda " . mysqli_error($mysqli));
      }
    } else {
      $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Error, contact support " . mysqli_error($mysqli));
    }

    include_once('../../functions/cierra_conexion.php');
  } else {
    $resultados[] = array("success"=> false, "type"=> "profile edit", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No session");
  }
	print json_encode($resultados);
?>
