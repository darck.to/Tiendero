<?php
  session_start();
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());
  
  include_once('../../functions/abre_conexion.php');

  //CARGAMOS LAS TIENDAS
  $sqlt = $mysqli->query("SELECT nom, re1, re2, re3, tel, niv, id_tie FROM tien_table WHERE fla = 1");
  if ($sqlt->num_rows > 0) {
    while ($rowt = $sqlt->fetch_assoc()) {
      $resultados[] = array("success"=> true, "flag"=> true, "nom"=> $rowt['nom'], "re1"=> $rowt['re1'], "re2"=> $rowt['re2'], "re3"=> $rowt['re3'], "tel"=> $rowt['tel'], "niv"=> $rowt['niv'], "tie"=> $rowt['id_tie']);
    }
  } else {
    $resultados[] = array("success"=> false, "message"=> "No tiendas");
  }

  //DE HABER USUARIO LO RECONOCEMOS
  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $resultados[] = array("success"=> true, "flag"=> false, "message"=> "Si sesion");
  } else {
    $resultados[] = array("success"=> false, "message"=> "No sesion");
  }
  include_once('../../functions/cierra_conexion.php');
  
	print json_encode($resultados);
?>
