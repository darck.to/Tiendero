<?php
  header("Access-Control-Allow-Origin: *");
	header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

	$auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $opc = mysqli_real_escape_string($mysqli,$_POST['opc']);

  //OPCION O NO LOGIN
  if ($opc == 0) {
    $resultados[] = array("success"=> true, "level"=> 0, "class"=> "user-profile", "txt"=> "Tu Perfil", "ref"=> "profile");
    $resultados[] = array("success"=> true, "level"=> 9, "class"=> "navbar-divider");
    $resultados[] = array("success"=> true, "level"=> 1, "class"=> "is-primary", "txt"=> "<b>Registrate</b>", "ref"=> "signin");
    $resultados[] = array("success"=> true, "level"=> 1, "class"=> "is-light", "txt"=> "Inicia Sesion", "ref"=> "login");
  } else {

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $sqlp = $mysqli->query("SELECT nom, ape, apm FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
      if ($sqlp->num_rows > 0) {
        $rowp = $sqlp->fetch_assoc();
        $resultados[] = array("success"=> true, "level"=> 0, "class"=> "user-profile", "txt"=> $rowp['nom'] ." ". $rowp['ape'] ." ". $rowp['apm'], "ref"=> "profile");
        $resultados[] = array("success"=> true, "level"=> 9, "class"=> "navbar-divider");
        $resultados[] = array("success"=> true, "level"=> 0, "class"=> "user-store", "txt"=> "Tienda Personal", "ref"=> "store");
        $resultados[] = array("success"=> true, "level"=> 0, "class"=> "user-products", "txt"=> "Productos", "ref"=> "products");
        $resultados[] = array("success"=> true, "level"=> 9, "class"=> "navbar-divider");
        $resultados[] = array("success"=> true, "level"=> 8, "class"=> "is-danger", "txt"=> "<b>Cerrar Sesion</b>", "ref"=> "logout");
      }
    } else {
      $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
    }

  }

	print json_encode($resultados);
  include_once('../../functions/cierra_conexion.php');

?>
