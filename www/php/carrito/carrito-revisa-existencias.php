<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../../functions/abre_conexion.php');
    include_once('../../functions/functions.php');    

    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);
    $car = mysqli_real_escape_string($mysqli, $_POST['carrito']);
    $tra = mysqli_real_escape_string($mysqli, $_POST['transaccion']);

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sql = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
        if ($sql->num_rows > 0) {
            $row = $sql->fetch_assoc();
            //INFORMACION A BUSCAR (ID DE TIENDA, PRODUCTO)
            $id_tie = carrito_consulta($row["id_per"],$car,$tra,"tienda");
            $id_pro = carrito_consulta($row["id_per"],$car,$tra,"producto");
            //BUSCAMOS LA EXISTENCIA DEL INVENTARIO
            $sqli = $mysqli->query("SELECT can FROM inve_table WHERE id_pro = '".$id_pro."' AND id_tie = '".$id_tie."'");
            if ($sqli->num_rows > 0) {
                $rowi = $sqli->fetch_assoc();
                //REVISAMOS SI LA CANTIDAD EN INVENTARIO DIFIERE DE LA CANTIDAD EN CARRITO
                $inventario = $rowi['can'];
                $resultados[] = array("success"=> true, "inventario"=> $inventario);
            } else {
                $resultados[] = array("success"=> false, "message"=> "Error en Inventario");
            }
        }
    } else {
        $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
    }
    
    print json_encode($resultados);
    include_once('../../functions/cierra_conexion.php');
?>