<?php
    session_start();
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../../functions/functions.php');
    include_once('../../functions/abre_conexion.php');

    //$nav = mysqli_real_escape_string($mysqli,$_POST['nav']);
    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);
    $carrito = mysqli_real_escape_string($mysqli,$_POST['carrito']);
    $cupon = mysqli_real_escape_string($mysqli,$_POST['cupon']);

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');

    $resultados = array();

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sql = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
        //lee usuario comprador
        if ($sql->num_rows > 0) {
            $row = $sql->fetch_assoc();
            //revisa el cupon
            $descuento = "na";
            if (carrito_cupon_flag($cupon)) {
                $cuponera = carrito_cupon_valor($cupon);
                $tipo = $cuponera[0];
                $descuento = $cuponera[1];
                $id_descuento = $cuponera[2];
                $file = '../../data/usr/' . $row['id_per'] . '/carrito/' . $carrito . '.json';
                $data = file_get_contents($file);
                $json = json_decode($data, true);
                foreach ($json as &$content) {
                    foreach ($content['productos'] as $key => $productos) {
                        if ($productos['tienda'] == $id_descuento || $productos['producto'] == $id_descuento ) {
                            $content['productos'][$key]['cupon'] = $id_descuento;
                            $content['productos'][$key]['descuento'] = $descuento;
                        }
                    }
                }
                $newJsonString = json_encode($json, JSON_PRETTY_PRINT);
                if (file_put_contents($file, $newJsonString)) {
                    $resultados[] = array("success"=> true, "message" => "Cupon agregado", "carrito" => $carrito);
                } else {
                    $resultados[] = array("success"=> true, "message" => "Cupon no guardado");
                }
            } else {
                $resultados[] = array("success"=> true, "message" => "Cupon no", "carrito" => $carrito);
            }
        }
    } else {
        $resultados[] = array("success"=> false, "message" => "Auth error", "carrito" => $carrito);
    }

    print json_encode($resultados);
    include_once('../../functions/cierra_conexion.php');
?>
