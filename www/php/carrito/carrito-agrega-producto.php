<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../../functions/functions.php');
    include_once('../../functions/abre_conexion.php');

    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);
    $tienda = mysqli_real_escape_string($mysqli,$_POST['tienda']);
    $producto = mysqli_real_escape_string($mysqli,$_POST['producto']);
    $cantidad = mysqli_real_escape_string($mysqli,$_POST['cantidad']);

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');

    $resultados = array();

    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sql = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr = '".$row['id_usr']."'");
        //lee usuario comprador
        if ($sql->num_rows > 0) {
            $row = $sql->fetch_assoc();
            $comprador = $row['id_per'];
            //busca al usuario vendedor
            $sql = $mysqli->query("SELECT id_per FROM tien_table WHERE id_tie = '".$tienda."'");
            if ($sql->num_rows > 0) {
                $row = $sql->fetch_assoc();
                $vendedor = $row['id_per'];
                //revisa si existen carritos activos
                $carritoActivo = carrito_carga_actual($comprador);
                //carrito no existe
                if (empty($carritoActivo)) {
                    //crea el carrito y sus identificadores
                    $id_car = generateRandomString(8) . "_CAR";
                    $id_tra = generateRandomString(8) . "_TRA";
                    //precio del producto
                    $precio = productos_obtener_value($tienda, $producto, "pre");
                    if ($sql = $mysqli->query("INSERT INTO carr_table(id_tra, id_car, id_pro, id_tie, v_per, c_per, pre, fec) VALUES ('".$id_tra."', '".$id_car."', '".$producto."', '".$tienda."', '".$vendedor."', '".$comprador."', $precio, '".$fechaActual."')")) {
                        //si se crea la esctructura de archivos del carrito, puede proseguir, si no borra la entrada en la bdd
                        $lista[] = array("transaccion"=> $id_tra, "vendedor"=> $vendedor, "comprador"=> $comprador, "tienda"=> $tienda, "producto"=> $producto, "cantidad"=> $cantidad, "precio"=> $precio, "cupon"=> null, "descuento" => 0, "fecha"=> $fechaActual);
                        $carrito[] = array("id"=> $id_car, "productos"=> $lista, "fecha"=> $fechaActual, "activo"=> true);
                        //crea el carrito de compra
                        if (carrito_crea($comprador, $vendedor, $id_car, $carrito)) {
                            $resultados[] = array("success"=> true, "message"=> "Alta de producto en carrito");
                        } else {
                            if ($mysqli->query("DELETE FROM carr_table WHERE id_tra = '".$id_tra."'")) {
                                $resultados[] = array("success"=> false, "message"=> "Falló creación de estructura en alta de producto, se eliminó el registro");
                            }
                        }
                    } else {
                        $resultados[] = array("success"=> false, "message"=> "Falló alta de producto");
                    }
                //el carrito si existe
                } else {
                    //SI EL PRODUCTO ES DUPLICADO EN EL CARRITO
                    if (carrito_agrega_duplicado($comprador, $carritoActivo, $tienda, $producto, $cantidad)) {
                        $resultados[] = array("success"=> true, "message"=> "Modificacion de producto en carrito");
                    } else {
                        $id_tra = generateRandomString(8) . "_TRA";
                        //precio del producto
                        $precio = productos_obtener_value($tienda, $producto, "pre");
                        if ($sql = $mysqli->query("INSERT INTO carr_table(id_tra, id_car, id_pro, id_tie, v_per, c_per, pre, fec) VALUES ('".$id_tra."', '".$carritoActivo."', '".$producto."', '".$tienda."', '".$vendedor."', '".$comprador."', $precio, '".$fechaActual."')")) {
                            //si se crea la esctructura de archivos del carrito, puede proseguir, si no borra la entrada en la bdd
                            $content = array("transaccion"=> $id_tra, "vendedor"=> $vendedor, "comprador"=> $comprador, "tienda"=> $tienda, "producto"=> $producto, "cantidad"=> $cantidad, "precio"=> $precio, "cupon"=> null, "descuento" => 0, "fecha"=> $fechaActual);
                            //modifica el carrito de compra
                            if (carrito_modifica($comprador, $carritoActivo, $content)) {
                                $resultados[] = array("success"=> true, "message"=> "Alta de producto en carrito");
                            } else {
                                if ($mysqli->query("DELETE FROM carr_table WHERE id_tra = '".$id_tra."'")) {
                                    $resultados[] = array("success"=> false, "message"=> "Falló creación de estructura en alta de producto, se eliminó el registro");
                                }
                            }
                        } else {
                            $resultados[] = array("success"=> false, "message"=> "Falló alta de producto");
                        }
                    }
                }
            } else {
                $resultados[] = array("success"=> false, "message"=> "No se encontró al vendedor");
            }
        }
    } else {
        $resultados[] = array("success"=> false, "message" => "Auth error");
    }

    print json_encode($resultados);
    include_once('../../functions/cierra_conexion.php');
?>
