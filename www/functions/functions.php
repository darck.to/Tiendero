<?php
	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZasdfgqwertzxcvbpoiuylkjhmn';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//FUNCTION COMPRUEBA EMAIL
  	function validaEmail($correo) {
	  if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	    else return false;
	}
	////////////////CARRITO//////////////////////////////
	//CREADOR DE ESTRUCUTRA DE DIRECTORIO PARA CARRITO
	function carrito_crea_estrutura($usuario, $tienda) {
		//CARPETA DE CARRITO
		$dir = '../../data/usr/' . $usuario . '/carrito/';
		$car = false;
		if(!is_dir($dir)) {
			if (mkdir($dir, 0755, true)) {
			  $car = true;
			}
		} else {
			$car = true;
		}
		if ($car) {
		  return true;
		} else {
		  return false;
		}
	}
	//FUNCTIONS PARA CARRITO
	function carrito_crea($comprador, $vendedor, $carrito, $content) {
		//CARPETA DE TIENDA Y PRODUCTOS
		$file = '../../data/usr/' . $comprador . '/carrito/' . $carrito . '.json';
		$json = fopen($file, 'w') or die ("error de lectura");
		$newJsonString = json_encode($content, JSON_PRETTY_PRINT);
		file_put_contents($file, $newJsonString);
		fclose($json);
		chmod($file, 0777);
		$tie = true;
		if ($tie) {
			return true;
		} else {
			return false;
		}
	}
	function carrito_borra_estructura($usuario, $tienda) {
		//CARPETA DE TIENDA Y PRODUCTOS
		$dir = '../../data/usr/' . $usuario . '/carrito/' . $carrito . '/';
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
			rmdir($file->getRealPath());
			} else {
			unlink($file->getRealPath());
			}
		}
		if (rmdir($dir)) {
			$tie = true;
		}
		if ($tie) {
			return true;
		} else {
			return false;
		}
	}
	function carrito_consulta($comprador, $carrito, $transaccion, $consulta) {
		//CARPETA DE TIENDA Y PRODUCTOS CONSULTAMOS INFORMACION DE LA CONSULTA
		$file = '../../data/usr/' . $comprador . '/carrito/' . $carrito . '.json';
		$data = file_get_contents($file);
		$json = json_decode($data, true);
		$returnValue = false;
		foreach ($json as $content) {
			foreach ($content['productos'] as $productos) {
				if ($productos['transaccion'] == $transaccion) {
					$returnValue = $productos[$consulta];
				}
			}
		}
		return $returnValue;
	}
	function carrito_modifica($comprador, $carrito, $producto) {
		//FUNCTION PARA LA INICIALIZACION Y MODIFICACION DE CONTENIDO A LOS ARCHIVOS DE CONFIGURACION JSON
		$file = '../../data/usr/' . $comprador . '/carrito/' . $carrito . '.json';
		$data = file_get_contents($file);
		$json = json_decode($data, true);
		array_push($json[0]["productos"], $producto);
		$newJsonString = json_encode($json, JSON_PRETTY_PRINT);
		if (file_put_contents($file, $newJsonString)) {
			return true;
		} else {
			return false;
		}
	}
	function carrito_agrega_duplicado($comprador, $carrito, $tienda, $producto, $cantidad) {
		//FUNCTION PARA AGREGAR CANTIDAD A LOS PRODUCTOS SI SON DUPLICADOS
		$file = '../../data/usr/' . $comprador . '/carrito/' . $carrito . '.json';
		$data = file_get_contents($file);
		$json = json_decode($data, true);
		foreach ($json as &$content) {
			foreach ($content['productos'] as &$productos) {
				if (strcmp($productos['producto'], $producto) == 0) {
					$productos['cantidad'] = $productos['cantidad'] + $cantidad;
					$modifica = true;
				} else {
					$modifica = false;
				}
			}
		}
		if ($modifica) {
			$newJsonString = json_encode($json, JSON_PRETTY_PRINT);
			if (file_put_contents($file, $newJsonString)) {
				$returnValue = true;
			} else {
				$returnValue = false;
			}
		} else {
			$returnValue = false;
		}
		return $returnValue;
	}
	function carrito_carga_actual($usuario) {
		//FUNCTION PARA LEÉR EL CARRITO DE COMPRAS ACTIVO DEL USUARIO
		$fileList = glob('../../data/usr/' . $usuario . '/carrito/*_CAR.json');
		$returnValue = false;
		foreach ($fileList as $filename) {
			if (file_exists($filename)) {	
				$data = file_get_contents($filename);
				$json = json_decode($data, true);
				foreach ($json as $content) {
					if ($content["activo"]) {
						$returnValue = $content["id"];
						return $returnValue;
					}
				}
			}
		}
	}
	function carrito_lista($comprador, $carrito) {
		//CARPETA DE TIENDA Y CARRITO
		$file = '../../data/usr/' . $comprador . '/carrito/' . $carrito . '.json';
		$data = file_get_contents($file);
		$json = json_decode($data, true);
		foreach ($json as &$content) {
			foreach ($content['productos'] as &$productos) {
				$nombreTienda = tienda_obtener_value($productos['tienda'],"nom");
				$nombreProducto = productos_obtener_value($productos['tienda'], $productos['producto'],"nom");
				$productos['tienda'] = $nombreTienda;
				$productos['producto'] = $nombreProducto;
			}
		}
		return $json;
	}
	function carrito_cupon_flag($cupon) {
		//CARPETA DE CUPONES
		$file = '../../data/cupones/' . $cupon . '_CUP.json';
		if (file_exists($file)) {
			return true;
		} else {
			return false;
		}
	}
	function carrito_cupon_valor($cupon) {
		//CARPETA DE CUPONES
		$file = '../../data/cupones/' . $cupon . '_CUP.json';
		if (file_exists($file)) {
			$data = file_get_contents($file);
			$json = json_decode($data, true);
			foreach ($json as $content) {
				if ($content['id'] == $cupon) {
					$descuento = $content['descuento'];
					$tipo = $content['tipo'];
					$id_descuento = $content['id_descuento'];
					return array($tipo, $descuento, $id_descuento);
				}
			}
		} else {
			return false;
		}
	}

	////////////////TIENDA//////////////////////////////
	//TIENDA
	function tienda_obtener_value($tienda, $consulta) {
		include('../../functions/abre_conexion.php');
		//FUNCTION PARA OBTENER PRECIO DEL PRODUCTO
		$sql = $mysqli->query("SELECT ".$consulta." as resultado FROM tien_table WHERE id_tie = '".$tienda."'");
		if ($sql->num_rows > 0) {
			$row = $sql->fetch_assoc();
			return $row['resultado'];
		} else {
			return false;
		}
		include('../../functions/cierra_conexion.php');
	}

	//PRODUCTOS
	function productos_obtener_value($tienda, $producto, $consulta) {
		include('../../functions/abre_conexion.php');
		//FUNCTION PARA OBTENER VALUES  DEL PRODUCTO
		$sql = $mysqli->query("SELECT ".$consulta." as resultado FROM inve_table WHERE id_tie = '".$tienda."' AND id_pro = '".$producto."'");
		if ($sql->num_rows > 0) {
			$row = $sql->fetch_assoc();
			return $row['resultado'];
		} else {
			return false;
		}
		include('../../functions/cierra_conexion.php');
	}
?>
