$(function() {
  //CARGA EL MAIN LOGIN
  carga_main_login()

  function carga_main_login(e) {
    var tie_key = localStorage.getItem("tie_key");
    var tie_user = localStorage.getItem("tie_user");
    var tie_log = localStorage.getItem("tie_log");
    //COMPROBAMOS SI EXISTE LOGIN LOCAL
    if (tie_log == 0 || tie_log == null) {
      //LOCAL LOGIN NULL OR NEGATIVE
      $.ajax({
        type: 'POST',
        url: 'http://localhost/tiendero/www/php/init/init-comprueba-auth.php',
        data: {
          auth : tie_key,
          user : tie_user
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          var aprobacion
          $.each(data, function (name, value) {
            if (value.success == true) {
              localStorage.setItem("tie_log",1);
              //INICIALIZA EL MENU
              cargaMenu();
              toast(value.message)
            } else {
              //INICIALIZA EL MENU
              cargaMenu();
              localStorage.removeItem("tie_key");
              localStorage.removeItem("tie_user");
              localStorage.setItem("tie_log",0)
            }
          });
        },
        error: function(xhr, tst, err) {
          toast('El tiempo de espera fue superado, por favor intentalo en un momento mas')
        }
      })
    } else {
      //INICIALIZA EL MENU
      cargaMenu()
    }
  }
  function cargaMenu(e) {
    $('#menu').load('templates/menu/menu.html')
  }
});
